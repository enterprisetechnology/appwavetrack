﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveTrack.Model
{
    class LoginModel
    {
            public int Id { get; set; }
            public string Email { get; set; }
            public string Senha { get; set; }
    }
}
