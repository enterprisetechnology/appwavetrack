﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveTrack.Database
{
    class LoginDatabase
    {
        public Model.LoginModel Login(string Email, string Senha)
        {
            string script = "select * from tb_usuario where em_email = @em_email and ps_password = @ps_password";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("em_email",Email));
            parms.Add(new MySqlParameter("ps_password", Senha));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            Model.LoginModel model = null;

            if (reader.Read())
            {
                model = new Model.LoginModel();
                model.Id = Convert.ToInt32(reader["id_user"]);
                model.Email = Convert.ToString(reader["em_email"]);
                model.Senha = Convert.ToString(reader["ps_password"]);
            }
            reader.Close();

            return model;
        }
    }
}
