﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveTrack.Database
{
    class CadastrarDatabase
    {
        public void Inserir(Model.CadastrarModel cadastrar)
        {
            string script = @"insert into tb_usuario (nm_user, ps_password, em_email)
                                          values  (@nm_user, @ps_password, @em_email)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_user", cadastrar.Nome));
            parms.Add(new MySqlParameter("ps_password", cadastrar.Senha));
            parms.Add(new MySqlParameter("em_email", cadastrar.Email));

            DB db = new DB();
            db.ExecuteInsertScript(script, parms);
        }
    }
}
