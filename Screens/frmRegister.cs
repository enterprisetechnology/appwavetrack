﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WaveTrack.Screens
{
    public partial class frmRegister : Form
    {
        public frmRegister()
        {
            InitializeComponent();
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            fmrLogin Login = new fmrLogin();
            Login.Show();
            this.Hide();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            try
            {
                string Nome = txtUsuario.Text;
                string Email = txtEmail.Text;
                string Senha = txtSenha.Text;

                Model.CadastrarModel cadastrar = new Model.CadastrarModel();
                cadastrar.Nome = Nome;
                cadastrar.Email = Email;
                cadastrar.Senha = Senha;

                Business.CadastrarBusiness cadastrarBusiness = new Business.CadastrarBusiness();
                cadastrarBusiness.Inserir(cadastrar);

                MessageBox.Show("Cadastro inserido com sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch ( Exception ex)
            {
                MessageBox.Show("Aconteceu um erro. Tente mais tarde");
            }
        }
    }
}
