﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WaveTrack
{
    public partial class fmrLogin : Form
    {
        public fmrLogin()
        {
            InitializeComponent();
        }

        private void LblRegister_Click(object sender, EventArgs e)
        {
            Screens.frmRegister Registrar = new Screens.frmRegister();
            Registrar.Show();
            this.Hide();
        }

        private void BtnClose_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string Email = txtUsuario.Text;
            string Senha = txtSenha.Text;

            Business.LoginBusiness loginBusiness = new Business.LoginBusiness();
            Model.LoginModel loginModel = loginBusiness.Login(Email, Senha);

            if (loginModel != null)
            {
                Model.LogadoModel.Id = loginModel.Id;
                Model.LogadoModel.Email = loginModel.Email;

                this.Close();
            }
            else
            {
                MessageBox.Show("Credenciais inválidas.", "Rid", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
