﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveTrack.Business
{
    class LoginBusiness
    {
            public Model.LoginModel Login(string Email, string Senha)
            {
                Database.LoginDatabase loginDB = new Database.LoginDatabase();
                Model.LoginModel login = loginDB.Login(Email, Senha);

                return login;
            }
        
    }
}
