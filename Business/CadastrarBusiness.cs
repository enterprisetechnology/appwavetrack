﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaveTrack.Business
{
    class CadastrarBusiness
    {
        public void Inserir(Model.CadastrarModel cadastrar)
        {
            if (cadastrar.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório");
            }

            Database.CadastrarDatabase cadastrarDatabase = new Database.CadastrarDatabase();
            cadastrarDatabase.Inserir(cadastrar);
        }
    }
}
